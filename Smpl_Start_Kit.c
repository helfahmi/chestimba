/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright (c) Nuvoton Technology Corp. All rights reserved.                                             */
/*                                                                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include "NUC1xx.h"
#include "Driver\DrvSYS.h"
#include "Driver\DrvGPIO.h"
#include "LCD_Driver.h"
#include "Seven_Segment.h"
#include "ScanKey.h"
#include "EEPROM_24LC64.h"

#define MICRO_SEC 1200
#define DELAY_MICRO_SEC 1200 * MICRO_SEC

/* Static constants */
static unsigned int elapsed_time = 0;
static unsigned char n_mapping[7] = { 0, 2, 3, 3, 2, 3, 3 };
static unsigned char mapping[7][3] = { {'_'}, { '0', '1' }, {'2','3','4'}, {'5','6','7'},{ '8', '9'}, {'A', 'B', 'C'}, {'D', 'E', 'F'} };
static char digits[9];
static int length;
static int page = 0;
static char pageNum[] = "Page  ";
static int isEnter =0;

void delay_loop(void)
 {
 uint32_t i;
	for(i=0;i< 2 *  MICRO_SEC;i++) elapsed_time+=2;
 
 }

/*----------------------------------------------------------------------------
  Interrupt subroutine
  ----------------------------------------------------------------------------*/
static unsigned char count=0;
static unsigned char loop=12;
void TMR0_IRQHandler(void) // Timer0 interrupt subroutine 
{ 
    unsigned char i=0;
 	TIMER0->TISR.TIF =1;
	count++;
	if(count==5)
	{
	   	DrvGPIO_ClrBit(E_GPC,loop);
	   	loop++;
	   	count=0;
	   	if(loop==17)
	   	{
	   		for(i=12;i<16;i++)
		   	{
	   			DrvGPIO_SetBit(E_GPC,i);	   
	   		}
			loop=12;
	   }
	}
}

//Mengambil string mulai dari index ke-startStringIndex sampai startStringIndex+8
//Harus sudah di init GPIO

int getString(int length, int startStringIndex){
	int digitIndex = 0;
	int i;
	
	if((startStringIndex - 1) + 4 <= length * 4){
		//ada
		
	} else { 
		//nggak ada
		return 0;
	}
	
	//Ambil key pertama
	for(i=startStringIndex;i<startStringIndex+4;i++){
		digits[digitIndex] = Read_24LC64(i);
		digitIndex++;
	}
	
	digits[digitIndex] = ' ';
	digitIndex++;
	
	//Cek key kedua ada apa nggak?
	if((startStringIndex - 1 + 4) + 4 <= length * 4){
		//ada
		
	} else { 
		//nggak ada
		digits[digitIndex] = '\0';
		return 1;
	}
	
	//Ambil key kedua
	for(i=startStringIndex+4;i<startStringIndex+8;i++){
		digits[digitIndex] = Read_24LC64(i);
		digitIndex++;
	}
	
	return 1;
}


int maxPageLength(){
	if(length <= 4)
		return 1;
	else {
		if((length-4) % 6 == 0){
			return ((length - 4)/6 + 1);
		} else {
			return ((length - 4)/6 + 2);
		}
	}
}

void Timer_initial(void)
{
	/* Step 1. Enable and Select Timer clock source */          
	SYSCLK->CLKSEL1.TMR0_S = 0;	//Select 12Mhz for Timer0 clock source 
    SYSCLK->APBCLK.TMR0_EN =1;	//Enable Timer0 clock source

	/* Step 2. Select Operation mode */	
	TIMER0->TCSR.MODE=1;		//Select periodic mode for operation mode

	/* Step 3. Select Time out period = (Period of timer clock input) * (8-bit Prescale + 1) * (24-bit TCMP)*/
	TIMER0->TCSR.PRESCALE=0;	// Set Prescale [0~(int)' ']
	TIMER0->TCMPR  = 1180000;		// Set TICR(TCMP) [0~16777215]
								// (1/22118400)*(0+1)*(2765)= 125.01usec or 7999.42Hz

	/* Step 4. Enable interrupt */
	TIMER0->TCSR.IE = 1;
	TIMER0->TISR.TIF = 1;		//Write 1 to clear for safty		
	NVIC_EnableIRQ(TMR0_IRQn);	//Enable Timer0 Interrupt

	/* Step 5. Enable Timer module */
	TIMER0->TCSR.CRST = 1;		//Reset up counter
	TIMER0->TCSR.CEN = 1;		//Enable Timer0

  	TIMER0->TCSR.TDR_EN=1;		// Enable TDR function
}


void clear_lcd(){
	print_lcd(0, "                 ");
	print_lcd(1, "                 ");
	print_lcd(2, "                 ");
	print_lcd(3, "                 ");
}

unsigned char findAddress() {
	return (Read_24LC64(0)*4)+1;
}

void refreshPage (){
	 clear_lcd();
	 pageNum[5] = 48 + page;
	 print_lcd(3, pageNum);

		if(page == 0){
			print_lcd(0, "Kunci tersimpan:");
			
			if(getString(length, 1) == 1){
				print_lcd(1, digits);
			}		   
			
			if(getString(length, 9) == 1){
				print_lcd(2, digits);
			}
			
		} else {
			if(getString(length, 17 + (page-1)*24) == 1){
				print_lcd(0, digits);
			} 
			
			if(getString(length, 25 + (page-1)*24) == 1){
				print_lcd(1, digits);
			} 
			
			if(getString(length, 33 + (page-1)*24) == 1){
				print_lcd(2, digits);
			}
		}							
		print_lcd(3, pageNum);
}


int main(void)
{
	/* Used variables */
	unsigned char dig[4] = {(int)' ', (int)' ', (int)' ', (int)' '};
	 int i=0, j=0;
	 int cur_pos = 0;
		unsigned char last_key = (int)' ';
		int key_is_pushed = 0;

	 
	/* Unlock the protected registers */	
	UNLOCKREG();
   	/* Enable the 12MHz oscillator oscillation */
	DrvSYS_SetOscCtrl(E_SYS_XTL12M, 1);
 
     /* Waiting for 12M Xtal stalble */
    SysTimerDelay(5000);
 
	/* HCLK clock source. 0: external 12MHz; 4:internal 22MHz RC oscillator */
	DrvSYS_SelectHCLKSource(0);		
    /*lock the protected registers */
	LOCKREG();				

	DrvSYS_SetClockDivider(E_SYS_HCLK_DIV, 0); /* HCLK clock frequency = HCLK clock source / (HCLK_N + 1) */

    for(i=12;i<16;i++)
	{		
		DrvGPIO_Open(E_GPC, i, E_IO_OUTPUT);
    }

	Initial_pannel();  //call initial pannel function
	clr_all_pannal();
	
	DrvGPIO_InitFunction(E_FUNC_I2C1);			  	

	length = Read_24LC64(0);
	
	refreshPage();
	
	Timer_initial();
	
	elapsed_time = 1500 * MICRO_SEC;
	cur_pos = -1;
	
	
	while(1)
	{
		unsigned char key = Scankey();
		
		/* Key on push */
		if (key > 0) 
		{
				/* Key is on hold */
				if (key_is_pushed > 0)
				{
					 // DO NOTHING
				}
				else 
				{
					 key_is_pushed = 1; // Set key as pushed
					 
					 /* Backspace key */
					 if (key == 7)  
					 {
							 dig[cur_pos] = (int)' ';
							 if (cur_pos >= 0) cur_pos--;
							 j = 0;
							 last_key = (int)' ';
					 }
					 else
					 /* Enter key */
					 if (key == 8)
					 { 
					 	 if ((last_key == 8) && (elapsed_time < MICRO_SEC * 500))
					 	{	
							if ((dig[0]==(int)' ')&&(dig[1]==(int)' ')&&(dig[2]==(int)' ')&&(dig[3]==(int)' ')) {
								 if (isEnter == 0)
								 {
									 Write_24LC64(0,0);
									 length = 0;
								 }
							} 
						  }
						  else
						  { 	  	  
						  	 if (dig[0] != (int)' ')
							 {
								Write_24LC64(findAddress(),dig[0]);
								Write_24LC64(findAddress()+1,dig[1]);
								Write_24LC64(findAddress()+2,dig[2]);
								Write_24LC64(findAddress()+3,dig[3]);
								Write_24LC64(0,Read_24LC64(0)+1);
								length=length+1;
								isEnter = 1;
							}
							else
								isEnter = 0;
						 }
							refreshPage();
							
							for (i = 0; i < 4; i++) dig[i] = (int)' ';
							last_key = 8;
							cur_pos = -1;
							j = 0;
							elapsed_time = 1500 * MICRO_SEC;
					 }
						
					 else 
					 /* Next page */ 
					 if (key == 9)
					 {
						 page = (page + 1) % maxPageLength();
						 refreshPage();
							
						 last_key = (int)' ';
						 elapsed_time = 1500 * MICRO_SEC;
					 }
					 else  
					/* Number/Alphabet key is pushed less than 1.5 sec diff */
					 if (elapsed_time < DELAY_MICRO_SEC)
					 {
								if (key == last_key) // Current key is the same as last,
								{	
									j = (j+1)% n_mapping[key];	
									 dig[cur_pos] = mapping[key][j]; 
								}
								else // Move to next digit
								{
									
									if (cur_pos < 3)
									{
									 cur_pos++;
									}
									j = 0;
									dig[cur_pos] = mapping[key][0];
									last_key = key;
								}
					 }
					 else
					 /* Push diff time >= 1.5 sec or key is differ from last */
					 {
							 if (cur_pos < 3) 
							{
								 cur_pos++;
							}
							j = 0;
							dig[cur_pos] = mapping[key][0];
							last_key = key;
					 }
					 
					 elapsed_time = 0; // Reset time diff
					 
				}
		}
		else
	  /* No key is pushed */
		{
				key_is_pushed = 0;
		}				
		
		/* Print seven segment */
		for(i=0;i<4;i++)
		{
		  close_seven_segment();
		  show_seven_segment(3-i,dig[i]);
		  delay_loop();		
		}
	}	  		
}


